# README #

### What is this repository for? ###

* This project is for a backoffice app that as an internal user, need to be able to update person/company data on the platform in order to prepare the data for demos and ensure the platform is shown in its best light also to be able to edit this data directly so that as an internal user we do not have to wait for the developers to do this.
* Version 0.0.1

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact