const Header = () => {
    return (
        <header>
            <h1>Drax Candidate Enrichment Portal</h1>
        </header>
    )
}

export default Header
