import { Button } from 'react-bootstrap'

const Navbar = () => {
    return (
        <div>
            <Button href="/company" type="button" variant="light">View Company Products/Sectors</Button>
            <Button href="/history" type="button" variant="light">View Person Work History</Button>
            <Button href="/person" type="button" variant="light">Create / Delete Person</Button>
        </div>
    )
}

export default Navbar
