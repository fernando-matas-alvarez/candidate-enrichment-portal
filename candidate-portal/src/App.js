import { BrowserRouter as Router, Route } from 'react-router-dom'

import Header from './components/Header'
import Navbar from './components/Navbar'
import Company from './components/Company'
import Person from './components/Person'
import WorkHistory from './components/WorkHistory'

function App() {

  return (

    <Router>
      <div className="container">
        <Header />
        <Navbar />
        <Route path='/' exact component={Company} />
        <Route path='/company' component={Company} />
        <Route path='/history' component={WorkHistory} />
        <Route path='/person' component={Person} />
      </div>
    </Router>
  );
}

export default App;
